package
{
	import flash.display.Sprite;
	import flash.geom.Matrix3D;
	import flash.geom.Orientation3D;
	import flash.geom.Vector3D;


	/**
	 *
	 * @author feng 2016-3-24
	 */
	public class test extends Sprite
	{
		public function test()
		{
			var str:String = "planes:Vector.<Plane3D>";
			trace(str.replace(/Vector.<([a-zA-Z0-9]+)>/g, "$1[]"));


//			var mat:Matrix3D = new Matrix3D();
//			mat.appendRotation(45, Vector3D.X_AXIS);
//			mat.appendRotation(65, Vector3D.Y_AXIS);
//			mat.appendRotation(64, Vector3D.Z_AXIS);
//
//			mat.appendScale(3.2, 4.5, 8.4);
//			mat.appendTranslation(100, 123, 456);
//			trace(mat.rawData)

//			var mat:Matrix3D = new Matrix3D(Vector.<Number>([0.7500000000000001, 0.6123724356957945, 0.24999999999999983, 0, -0.6123724356957945, 0.5000000000000002, 0.6123724356957945, 0, 0.24999999999999983, -0.6123724356957945, 0.75, 0, 0.1516126864206031, 1.2496888977739185, -0.15161268642060288, 1]));
//			var mat1:Matrix3D = new Matrix3D(Vector.<Number>([1, 2, 6, 4, 5, 6, 17, 8, 9, 10, 11, 12, 13, 13, 15, 18]));
//			mat.appendTranslation(3, 4, 5);
//			var vec:Vector3D = new Vector3D(5, 6, 7, 5);
//			mat.copyColumnTo(1, vec);

//			var vec:Vector.<Number> = new Vector.<Number>()
//			for (var i:int = 0; i < 1; i++)
//			{
//				vec.push(i * 4 + 1, i * 4 + 2, i * 4 + 3, i * 4 + 4);
//			}

//			mat.copyRawDataFrom(vec, 2, true);

//			mat.copyRowFrom(0, vec);
//			mat.copyRowTo(1, vec);

//			trace(mat.rawData);
//			trace(vec);

//			var vec1 = mat.decompose();
//
//			trace(vec1);

//			mat = new Matrix3D();
//			mat.appendRotation(30, new Vector3D(1, 0, 1), new Vector3D(1, 2, 3));
//			mat.appendRotation(30, new Vector3D(1, 0, 1));

//			mat.appendScale(2.1, -3.2, 5.4);
//			mat.prependScale(2.1, -3.2, 5.4);

//			var vec:Vector3D = new Vector3D(1, 2, 3);
////			var vec2:Vector3D = mat.transformVector(vec);
//			var vec2:Vector3D = mat.deltaTransformVector(vec);
//
//			trace(vec, vec2);
//
//			trace(mat.rawData);

			testf();
		}

		private function testf():void
		{
			var mat:Matrix3D = new Matrix3D(Vector.<Number>([0.7500000000000001, 0.6123724356957945, 0.24999999999999983, 0, -0.6123724356957945, 0.5000000000000002, 0.6123724356957945, 0, 0.24999999999999983, -0.6123724356957945, 0.75, 0, 0.1516126864206031, 1.2496888977739185, -0.15161268642060288, 1]));
			var vec:Vector.<Vector3D> = mat.decompose(Orientation3D.EULER_ANGLES);
			trace(vec);
			trace(mat.rawData);
			mat.identity();
			mat.appendRotation(vec[1].x * 180 / Math.PI, Vector3D.X_AXIS);
			mat.appendRotation(vec[1].y * 180 / Math.PI, Vector3D.Y_AXIS);
			mat.appendRotation(vec[1].z * 180 / Math.PI, Vector3D.Z_AXIS);
			mat.appendScale(vec[2].x, vec[2].y, vec[2].z);
			mat.appendTranslation(vec[0].x, vec[0].y, vec[0].z);

//			mat.recompose(vec, Orientation3D.EULER_ANGLES);
			trace(mat.rawData);

		}
	}
}
